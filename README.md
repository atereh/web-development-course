# Web development course at ITMO, 6 semester #

### 1st lab ###

* Landing page for SCAMT project on "Organic and Organometallic Compounds"

### 2nd lab ###

* Rendering performance of loader page, according to pixels-to-screen pipeline

### 3rd lab ###

* Some basic JS tasks

### 4th lab ###

* Single Page Application with information about me + my pokemon, using PokeAPI 