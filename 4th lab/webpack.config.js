const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: './src/main.js',
  plugins: [
    new HtmlWebpackPlugin({template: './static/index.html'})
  ],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'out'),
  },
  devServer: {
    stats: 'errors-only',
    contentBase: path.join(__dirname, 'out'),
    compress: true,
    port: 3000
  }
};