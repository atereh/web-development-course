import clear_content from './clear_content'

const nameDiv = document.createElement('div')
const groupDiv = document.createElement('div')

nameDiv.innerHTML = `Терехова Алина Станиславна`
groupDiv.innerHTML = `M3307`


export default function set_content_about_me(target) {
    clear_content(target)
    target.append(nameDiv, groupDiv)
}