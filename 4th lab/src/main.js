import set_content_about_me from './me.js'
import set_pokemon_data from './pokemon.js'

const main = document.querySelector('#main')
const app = document.createElement('div')
const tabList = document.createElement('ul')
const info = document.createElement('div')

// Me
const li = document.createElement('li')
const link = document.createElement('a')
link.setAttribute('href', '#')
li.appendChild(link)
link.innerText = "Me"
link.addEventListener('click', async () => {
    set_content_about_me(info)
  })

// Pokemon
const li2 = document.createElement('li')
const link2 = document.createElement('a')
link2.setAttribute('href', '#')
li2.appendChild(link2)
link2.innerText = "Pokemon"
link2.addEventListener('click', async () => {
    let data = await (await fetch(`https://pokeapi.co/api/v2/pokemon/2/`)).json()
    set_pokemon_data(info, data)
  })

tabList.appendChild(li)
tabList.appendChild(li2)

app.appendChild(tabList)
app.appendChild(info)
main.appendChild(app)