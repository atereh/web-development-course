/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/clear_content.js":
/*!******************************!*\
  !*** ./src/clear_content.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return clear_content; });\nfunction clear_content(target) {\n    target.querySelectorAll('*').forEach((child) => {child.remove()})\n}\n\n//# sourceURL=webpack:///./src/clear_content.js?");

/***/ }),

/***/ "./src/main.js":
/*!*********************!*\
  !*** ./src/main.js ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _me_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./me.js */ \"./src/me.js\");\n/* harmony import */ var _pokemon_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pokemon.js */ \"./src/pokemon.js\");\n\n\n\nconst main = document.querySelector('#main')\nconst app = document.createElement('div')\nconst tabList = document.createElement('ul')\nconst info = document.createElement('div')\n\n// Me\nconst li = document.createElement('li')\nconst link = document.createElement('a')\nlink.setAttribute('href', '#')\nli.appendChild(link)\nlink.innerText = \"Me\"\nlink.addEventListener('click', async () => {\n    Object(_me_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(info)\n  })\n\n// Pokemon\nconst li2 = document.createElement('li')\nconst link2 = document.createElement('a')\nlink2.setAttribute('href', '#')\nli2.appendChild(link2)\nlink2.innerText = \"Pokemon\"\nlink2.addEventListener('click', async () => {\n    let data = await (await fetch(`https://pokeapi.co/api/v2/pokemon/2/`)).json()\n    Object(_pokemon_js__WEBPACK_IMPORTED_MODULE_1__[\"default\"])(info, data)\n  })\n\ntabList.appendChild(li)\ntabList.appendChild(li2)\n\napp.appendChild(tabList)\napp.appendChild(info)\nmain.appendChild(app)\n\n//# sourceURL=webpack:///./src/main.js?");

/***/ }),

/***/ "./src/me.js":
/*!*******************!*\
  !*** ./src/me.js ***!
  \*******************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return set_content_about_me; });\n/* harmony import */ var _clear_content__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./clear_content */ \"./src/clear_content.js\");\n\n\nconst nameDiv = document.createElement('div')\nconst groupDiv = document.createElement('div')\n\nnameDiv.innerHTML = `Терехова Алина Станиславна`\ngroupDiv.innerHTML = `M3307`\n\n\nfunction set_content_about_me(target) {\n    Object(_clear_content__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(target)\n    target.append(nameDiv, groupDiv)\n}\n\n//# sourceURL=webpack:///./src/me.js?");

/***/ }),

/***/ "./src/pokemon.js":
/*!************************!*\
  !*** ./src/pokemon.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return set_pokemon_data; });\n/* harmony import */ var _clear_content__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./clear_content */ \"./src/clear_content.js\");\n\n\nfunction set_pokemon_data(target, data) {\n    Object(_clear_content__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(target)\n    console.log(data)\n\n    // Abilities\n    const abilities = document.createElement('div')\n    abilities.textContent = 'Abilities: '\n    const abilitiesUl = document.createElement('ul')\n    abilities.appendChild(abilitiesUl)\n\n    data.abilities.forEach((ability) => {\n        const abilityLi = document.createElement('li')\n        abilityLi.textContent = `${ability.ability.name} ${\n        ability.is_hidden ? '(hidden)' : ''\n        }`\n        abilitiesUl.appendChild(abilityLi)\n    })\n\n    target.appendChild(abilities)\n\n    //Sprite image - to be fixed ?\n    /*const generateSprites = (data) => {\n        console.log(data)\n        const sprites = `\n        <img src=${data.sprites.front_default}>\n                        `\n        const pokemonImg = document.querySelector('.pokemon')\n        pokemonImg.innerHTML = sprites\n    }*/\n\n    const baseExperience = document.createElement('div')\n    baseExperience.textContent = `Base experience: ${data.base_experience}`\n    target.appendChild(baseExperience)\n\n    const species = document.createElement('div')\n    species.textContent = `Species: ${data.species.name}`\n    target.appendChild(species)\n\n    // Types\n    const types = document.createElement('div')\n    const typesUl = document.createElement('ul')\n    types.textContent = `Types: `\n    types.appendChild(typesUl)\n\n    data.types.forEach((typeEntity) => {\n        const typeLi = document.createElement('li')\n        typeLi.textContent = `${typeEntity.type.name} (${typeEntity.slot})`\n        typesUl.appendChild(typeLi)\n    })\n    target.appendChild(types)\n}\n\n\n//# sourceURL=webpack:///./src/pokemon.js?");

/***/ })

/******/ });