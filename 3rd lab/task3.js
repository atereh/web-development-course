const object1 = {
	name: 'Boris',
	weight: 15
};

const object2 = {
	name: 'Boris',
	weight: 15
};

const object3 = {
	name: 'Rex',
	weight: 20,
};

//isEquivalent function here 

function isEquivalent(object1, object2){

	for(let field in object1) {
        if(object1[field] !== object2[field]) {
            return false;
        }
    }

    return true;

}

console.log(isEquivalent(object1, object2)); // true
console.log(isEquivalent(object1, object3)); // false