function createCounter() {
	let i = 1;
	return function count()
	{
		return i++;
	}
    //counter realisation
}

const count = createCounter();

count(); // 1
count(); // 2
count(); // 3
count(); // 4

console.log(count()); // Expected answer: 5